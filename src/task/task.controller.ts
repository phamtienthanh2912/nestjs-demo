import { Controller, Get, Post, Put, Delete, Body, Param, HttpCode, Header , Res, HttpStatus, UseGuards, HttpException} from '@nestjs/common';
import { TaskService } from './task.service';
import { Task } from './task.entity';
import { Response } from 'express';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard('jwt'))
@Controller('task')
export class TaskController {
    constructor(private readonly taskService: TaskService) {

    }

    @Get('/abc')
    findA(@Res() res: Response) {
        throw new HttpException({
            status: HttpStatus.FORBIDDEN,
            error: 'This is a custom message',
          }, 403);
        //res.status(HttpStatus.OK).json(['ac']);
    }

    @Get()
    @HttpCode(200)
    findAll(): Promise<Task[]> {
        return this.taskService.findAll();
    }

    @Get(':id')
    get(@Param() params) {
        return this.taskService.findOne(params.id);
    }

    @Post()
    create(@Body() task: Task) {
        return this.taskService.create(task);
    }

    @Put()
    update(@Body() task: Task) {
        return this.taskService.update(task);
    }

    @Delete(':id')
    deleteUser(@Param() params) {
        return this.taskService.delete(params.id);
    }
}
