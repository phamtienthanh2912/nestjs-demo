import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { HttpExceptionFilter } from './exception/HttpExceptionFilter';
import * as helmet from 'helmet';
import * as csurf from 'csurf';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.disable('x-powered-by');
  app.useGlobalFilters(new HttpExceptionFilter());
  app.use(helmet());
  app.use(csurf());
  app.enableCors();
  // body parser
  app.use(bodyParser.json({ limit: '50mb' }));
  await app.listen(3000);
}
bootstrap();
