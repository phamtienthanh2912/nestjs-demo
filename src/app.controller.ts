import { Controller, Get, Body, Request, Post, UseGuards, Res } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth/auth.service';
import { AppService } from './app.service';
import { User } from './users/user.entity';
import { UsersService } from './users/users.service';

@Controller('api')
export class AppController {
  constructor(private readonly authService: AuthService,
              private readonly appService: AppService,
              private readonly userService: UsersService  ) { }

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  // @UseGuards(AuthGuard('jwt'))
  // @Get('me')
  // getProfile(@Request() req) {
  //   return req.user;
  // }

  // @Post('login')
  // async login(@Body() user: User): Promise<any> {
  //   return this.authService.login(user);
  // }

  @Post('register')
  async register(@Body() user: User): Promise<any> {
    return this.appService.register(user);
    // return this.authService.register(user);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }

  // @Get('/abc')
  //   findA(@Res() res: Response) {
        
  //       res.status(HttpStatus.OK).json(['ac']);
  //   }
}
