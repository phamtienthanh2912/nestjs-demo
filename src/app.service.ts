import { Injectable } from '@nestjs/common';
import { User } from './users/user.entity';
import { AuthService } from './auth/auth.service';

@Injectable()
export class AppService {

  constructor(private readonly authService: AuthService ) {}

  getHello(): string {
    return 'Hello World!';
  }

  public async register(user: User): Promise<any> {
    if (!user) {
      return { message: 'user ko duoc null'};
    } else {
      return this.authService.register(user);
    }
  }
}
