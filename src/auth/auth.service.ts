import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../users/user.entity';
import { CryptoUtil } from '../utils/CryptoUtil';
@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) { }

  private async validate(userData: User): Promise<User> {
    return await this.usersService.findByEmail(userData.email);
  }

  public async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findByUsername(username);
    const cry = new CryptoUtil();
    const passTmp = cry.mahoa(pass);
    if (user && user.password === passTmp) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  // public async login(user: User): Promise<any | { status: number }> {
  //   return this.validate(user).then(result => {
  //     if (!result) {
  //       return {status : 404};
  //     }

  //     const payload = `${result.username}${result.userId}`;
  //     const accessToken = this.jwtService.sign(payload);
  //     return {
  //       expires_in: 3600,
  //       access_token: accessToken,
  //       user_id: payload,
  //       status: 200,
  //    };
  //   });
  // }

  async login(user: any) {
    const payload = { username: user.username, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
      user_id: payload,
      status: 200,
    };
  }
  public async register(user: User): Promise<any> {
    if (user) {
      const tmp = await this.usersService.findByUsername(user.username);
      if (tmp) {
        return { message: 'Da ton tai user trong he thong' };
      } else {
        const cry = new CryptoUtil();
        user.password = cry.mahoa(user.password);
        return this.usersService.create(user);
      }
    }
  }
}
