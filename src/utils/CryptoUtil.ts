import * as crypto from 'crypto';

export class CryptoUtil {

    public mahoa(value: string): string {
        return crypto.createHmac('sha256', value).digest('hex');
    }
}
