import { Column, Entity, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate } from 'typeorm';
import * as crypto from 'crypto';

@Entity()
export class User {

  @PrimaryGeneratedColumn()
  userId: number;

  @Column()
  username: string;

  @Column()
  email: string;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    this.password = await crypto.createHmac('sha256', this.password).digest('hex');
  }

  @Column()
  password: string;

}
