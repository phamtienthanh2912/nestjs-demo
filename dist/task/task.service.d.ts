import { Task } from './task.entity';
import { Repository } from 'typeorm';
import { UpdateResult, DeleteResult } from 'typeorm';
export declare class TaskService {
    private readonly taskRepo;
    constructor(taskRepo: Repository<Task>);
    findAll(): Promise<Task[]>;
    findOne(id: number): Promise<Task>;
    create(task: Task): Promise<Task>;
    update(task: Task): Promise<UpdateResult>;
    delete(id: any): Promise<DeleteResult>;
}
