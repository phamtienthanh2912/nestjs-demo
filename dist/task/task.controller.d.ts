import { TaskService } from './task.service';
import { Task } from './task.entity';
import { Response } from 'express';
export declare class TaskController {
    private readonly taskService;
    constructor(taskService: TaskService);
    findA(res: Response): void;
    findAll(): Promise<Task[]>;
    get(params: any): Promise<Task>;
    create(task: Task): Promise<Task>;
    update(task: Task): Promise<import("typeorm").UpdateResult>;
    deleteUser(params: any): Promise<import("typeorm").DeleteResult>;
}
