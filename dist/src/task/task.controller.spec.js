"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@nestjs/testing");
const task_controller_1 = require("./task.controller");
describe('Task Controller', () => {
    let controller;
    beforeEach(async () => {
        const module = await testing_1.Test.createTestingModule({
            controllers: [task_controller_1.TaskController],
        }).compile();
        controller = module.get(task_controller_1.TaskController);
    });
    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
//# sourceMappingURL=task.controller.spec.js.map