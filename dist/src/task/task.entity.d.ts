export declare class Task {
    id: number;
    name: string;
    description: string;
    isDone: boolean;
}
